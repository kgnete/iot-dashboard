
/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var FacebookStrategy = require('passport-facebook').Strategy;
var config = require('config');
var User = mongoose.model('User');

/**
 * Expose
 */

module.exports = new FacebookStrategy({
    clientID: config.facebook.clientID,
    clientSecret: config.facebook.clientSecret,
    callbackURL: config.facebook.callbackURL,
    profileFields: ['id', 'displayName', 'photos', 'email']
  },
  function(accessToken, refreshToken, profile, done) {
    var options = {
      criteria: { 'facebook.id': profile.id }
    };
    User.load(options, function (err, user) {
      if (err) return done(err);
      if (!user) {
        console.log("profile: ", profile);
        user = new User({
          name: profile.displayName,
          email: profile.emails[0].value,
          username: profile.emails[0].value,
          provider: 'facebook',
          facebook: profile._json,
          password: (Math.round((new Date().valueOf() * Math.random())) + '')
        });
        user.save(function (err) {
          if (err) console.log(err);
          return done(err, user);
        });
      } else {
        if (!user.username || !user.hashed_password) {
            if (!user.username) user.username = profile.emails[0].value;
            if (!user.hashed_password) {
                user.password = Math.round((new Date().valueOf() * Math.random())) + '';
            }
            user.save();
        }
        return done(err, user);
      }
    });
  }
);
