/**
 * Home
 */

$(document).ready(function () {
    clientTime = new Date().getTime();
    clientId = "global:" + clientTime;
    var port = Number(location.port);
    if (port == 0) port = 443;
    client = new Paho.MQTT.Client(location.hostname, port, clientId);
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;
    client.connect({
        useSSL: true,
        onSuccess : onConnect,
        keepAliveInterval: 120
    });
});
function onConnect() {
    topic = "iot/global";
    client.subscribe(topic);
    console.log("Subscibed Topic: " + topic);
}
// called when the client loses its connection
function onConnectionLost(responseObject) {
    if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost: " + responseObject.errorMessage);
    }
}

// called when a message arrives
function onMessageArrived(message) {
    if (message.payloadString) {
        console.log(new Date().getTime() + " - " + message.payloadString);
        if (message.payloadString) {
            console.log(new Date().getTime() + " - " + message.payloadString);
            $("#messages").html(message.payloadString.split("=")[1]);
        }
    }
}
